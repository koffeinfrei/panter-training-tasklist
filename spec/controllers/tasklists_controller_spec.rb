require 'spec_helper'

describe TasklistsController do

  # This should return the minimal set of attributes required to create a valid
  # Tasklist. As you add validations to Tasklist, be sure to
  # update the return value of this method accordingly.
  def valid_attributes
    { "title" => "MyString" }
  end

  # This should return the minimal set of values that should be in the session
  # in order to pass any filters (e.g. authentication) defined in
  # TasklistsController. Be sure to keep this updated too.
  def valid_session
    {}
  end

  describe "GET index" do
    it "assigns all tasklists as @tasklists" do
      tasklist = Tasklist.create! valid_attributes
      get :index, {}, valid_session
      assigns(:tasklists).should eq([tasklist])
    end
  end

  describe "GET show" do
    it "assigns the requested tasklist as @tasklist" do
      tasklist = Tasklist.create! valid_attributes
      get :show, {:id => tasklist.to_param}, valid_session
      assigns(:tasklist).should eq(tasklist)
    end
  end

  describe "GET new" do
    it "assigns a new tasklist as @tasklist" do
      get :new, {}, valid_session
      assigns(:tasklist).should be_a_new(Tasklist)
    end

    it "the tasklist has 4 task items" do
      tasklist = Tasklist.create! valid_attributes
      get :new, {}, valid_session
      assigns(:tasklist).tasks.should have(4).items
    end
  end

  describe "GET edit" do
    it "assigns the requested tasklist as @tasklist" do
      tasklist = Tasklist.create! valid_attributes
      get :edit, {:id => tasklist.to_param}, valid_session
      assigns(:tasklist).should eq(tasklist)
    end

    it "the tasklist has 4 task items" do
      tasklist = Tasklist.create! valid_attributes
      get :edit, {:id => tasklist.to_param}, valid_session
      assigns(:tasklist).tasks.should have(4).items
    end
  end

  describe "POST create" do
    describe "with valid params" do
      it "creates a new Tasklist" do
        expect {
          post :create, {:tasklist => valid_attributes}, valid_session
        }.to change(Tasklist, :count).by(1)
      end

      it "assigns a newly created tasklist as @tasklist" do
        post :create, {:tasklist => valid_attributes}, valid_session
        assigns(:tasklist).should be_a(Tasklist)
        assigns(:tasklist).should be_persisted
      end

      it "assigns a newly created task item" do
        post :create, {:tasklist => valid_attributes.merge({:tasks_attributes => [{:title => "task 1"}]})}, valid_session
        assigns(:tasklist).tasks.first.title.should == "task 1"
      end

      it "redirects to the created tasklist" do
        post :create, {:tasklist => valid_attributes}, valid_session
        response.should redirect_to(Tasklist.last)
      end
    end

    describe "with invalid params" do
      it "assigns a newly created but unsaved tasklist as @tasklist" do
        # Trigger the behavior that occurs when invalid params are submitted
        Tasklist.any_instance.stub(:save).and_return(false)
        post :create, {:tasklist => { "title" => "invalid value" }}, valid_session
        assigns(:tasklist).should be_a_new(Tasklist)
      end

      it "re-renders the 'new' template" do
        # Trigger the behavior that occurs when invalid params are submitted
        Tasklist.any_instance.stub(:save).and_return(false)
        post :create, {:tasklist => { "title" => "invalid value" }}, valid_session
        response.should render_template("new")
      end
    end
  end

  describe "PUT update" do
    describe "with valid params" do
      it "updates the requested tasklist" do
        tasklist = Tasklist.create! valid_attributes
        # Assuming there are no other tasklists in the database, this
        # specifies that the Tasklist created on the previous line
        # receives the :update_attributes message with whatever params are
        # submitted in the request.
        Tasklist.any_instance.should_receive(:update_attributes).with({ "title" => "MyString" })
        put :update, {:id => tasklist.to_param, :tasklist => { "title" => "MyString" }}, valid_session
      end

      it "assigns the requested tasklist as @tasklist" do
        tasklist = Tasklist.create! valid_attributes
        put :update, {:id => tasklist.to_param, :tasklist => valid_attributes}, valid_session
        assigns(:tasklist).should eq(tasklist)
      end

      it "redirects to the tasklist" do
        tasklist = Tasklist.create! valid_attributes
        put :update, {:id => tasklist.to_param, :tasklist => valid_attributes}, valid_session
        response.should redirect_to(tasklist)
      end

      it "assigns a newly created task item" do
        tasklist = Tasklist.create! valid_attributes
        put :update, {:id => tasklist.to_param, :tasklist => {:tasks_attributes => [{:title => "task 1"}]}}, valid_session
        assigns(:tasklist).tasks.first.title.should == "task 1"
      end

      it "updates a existing task item" do
        tasklist = Tasklist.create! valid_attributes
        task = tasklist.tasks.build(:title => "task 1")
        tasklist.save

        put :update, {:id => tasklist.to_param, :tasklist => {:tasks_attributes => [{:id => task.to_param, :title => "task 2"}]}}, valid_session
        assigns(:tasklist).tasks.first.title.should == "task 2"
      end

      it "destroys the requested task" do
        tasklist = Tasklist.create! valid_attributes
        task = tasklist.tasks.build(:title => "task 1", :id => 1)
        task.save
        expect {
          put :update, {:id => tasklist.to_param, :tasklist => {:tasks_attributes => [{:id => task.to_param, :title => task.title, :_destroy => true}]}}, valid_session
        }.to change(Task, :count).by(-1)
      end
    end

    describe "with invalid params" do
      it "assigns the tasklist as @tasklist" do
        tasklist = Tasklist.create! valid_attributes
        # Trigger the behavior that occurs when invalid params are submitted
        Tasklist.any_instance.stub(:save).and_return(false)
        put :update, {:id => tasklist.to_param, :tasklist => { "title" => "invalid value" }}, valid_session
        assigns(:tasklist).should eq(tasklist)
      end

      it "re-renders the 'edit' template" do
        tasklist = Tasklist.create! valid_attributes
        # Trigger the behavior that occurs when invalid params are submitted
        Tasklist.any_instance.stub(:save).and_return(false)
        put :update, {:id => tasklist.to_param, :tasklist => { "title" => "invalid value" }}, valid_session
        response.should render_template("edit")
      end
    end
  end

  describe "DELETE destroy" do
    it "destroys the requested tasklist" do
      tasklist = Tasklist.create! valid_attributes
      expect {
        delete :destroy, {:id => tasklist.to_param}, valid_session
      }.to change(Tasklist, :count).by(-1)
    end

    it "redirects to the tasklists list" do
      tasklist = Tasklist.create! valid_attributes
      delete :destroy, {:id => tasklist.to_param}, valid_session
      response.should redirect_to(tasklists_url)
    end
  end

end
