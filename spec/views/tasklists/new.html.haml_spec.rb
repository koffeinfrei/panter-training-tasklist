require 'spec_helper'

describe "tasklists/new" do
  before(:each) do
    @tasklist = assign(:tasklist, stub_model(Tasklist,
      :title => "MyString"
    ).as_new_record)
  end

  it "renders new tasklist form" do
    render

    # Run the generator again with the --webrat flag if you want to use webrat matchers
    assert_select "form[action=?][method=?]", tasklists_path, "post" do
      assert_select "input#tasklist_title[name=?]", "tasklist[title]"
    end
  end

  it "renders the nested item fields" do
    @tasklist.tasks << stub_model(Task)

    render

    assert_select "form[action=?][method=?]", tasklists_path, "post" do
      assert_select "input#tasklist_tasks_attributes_0_title[type=?]", "text"
      assert_select "input#tasklist_tasks_attributes_0_done[type=?]", "checkbox"
    end
  end
end
