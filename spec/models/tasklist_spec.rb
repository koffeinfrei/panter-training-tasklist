require 'spec_helper'

describe Tasklist do
  it "does not save blank tasks" do
    tasklist = Tasklist.new :tasks_attributes => [{:title => nil}]
    expect{
      tasklist.save
    }.to_not change{ Task.count }
  end

  it "does save task with title" do
    tasklist = Tasklist.new :tasks_attributes => [{:title => "task 1"}]
    expect{
      tasklist.save
    }.to change{ Task.count }
  end
end
